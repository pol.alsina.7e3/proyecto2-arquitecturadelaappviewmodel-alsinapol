package com.example.affirmations

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Email
import androidx.compose.material.icons.rounded.Mail
import androidx.compose.material.icons.rounded.Phone
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.modifier.modifierLocalConsumer
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.affirmations.data.Datasource
import com.example.affirmations.model.Affirmation
import com.example.affirmations.ui.theme.AffirmationsTheme
import com.example.affirmations.viewModel.DetailViewModel
import com.example.affirmations.viewModel.MainViewModel

class DetailActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val intValue = intent.getIntExtra("id", 0)

        setContent {
            AffirmationAppDetail(intValue)
        }

    }
}

@Composable
fun AffirmationAppDetail(id: Int) {
    val viewModel: DetailViewModel = viewModel()
    viewModel.getCard(id)
    val uiState by viewModel.uiState.collectAsState()
    if (uiState != null) {
        AffirmationDetail(affirmation = uiState!!)
    }
}

@Composable
fun AffirmationDetail(affirmation: Affirmation) {
    val context = LocalContext.current
    AffirmationsTheme() {
        Column(
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier.fillMaxWidth()
        ) {
            Image(
                painter = painterResource(id = affirmation.imageResourceId),
                contentDescription = "image",
                modifier = Modifier
                    .padding(16.dp)
                    .size(250.dp)
                    .clip(RoundedCornerShape(50)),
                contentScale = ContentScale.Crop,

                )
            Text(
                text = stringResource(id = affirmation.stringResourceId),
                textAlign = TextAlign.Center,
                style = MaterialTheme.typography.h4,
                modifier = Modifier.padding(8.dp)
            )
            Text(
                text = AnnotatedString("Description: "),
                style = MaterialTheme.typography.h5,
                modifier = Modifier.padding(8.dp)
            )
            Text(
                text = stringResource(id = affirmation.stringDescription),
                style = MaterialTheme.typography.body1,
                modifier = Modifier.padding(8.dp)
            )
            Row(horizontalArrangement = Arrangement.End,modifier = Modifier.fillMaxWidth()) {
                TextButton(onClick = {
                    context.sendMail("","")
                },) {
                    Icon(
                        Icons.Rounded.Mail,
                        contentDescription = "send Mail",
                        modifier = Modifier
                            .padding(16.dp)
                            .size(25.dp)
                    )
                }
                TextButton(onClick = {
                    context.dial(affirmation.phoneNumber)
                }) {
                    Icon(
                        Icons.Rounded.Phone,
                        contentDescription = "Phone",
                        modifier = Modifier
                            .padding(16.dp)
                            .size(25.dp)
                    )
                }
            }
        }
    }
}
fun Context.sendMail( to: String, subject: String){
    try {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "message/rfc822"
        intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(to))
        intent.putExtra(Intent.EXTRA_SUBJECT, subject)
        startActivity(intent)
    } catch (t: Throwable){
        println("No funciona")
    }
}

fun Context.dial(phoneNum: String){
    try {
        val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phoneNum, null))
        startActivity(intent)
    } catch (t: Throwable){
        println("No funciona")
    }
}
@Preview
@Composable
private fun AffirmationCardPreview() {
    // TODO 2. Preview your card

}