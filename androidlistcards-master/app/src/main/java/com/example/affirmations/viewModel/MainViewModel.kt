package com.example.affirmations.viewModel

import androidx.lifecycle.ViewModel
import com.example.affirmations.data.Datasource
import com.example.affirmations.model.Affirmation
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow

class MainViewModel: ViewModel() {
    private val _uiSate = MutableStateFlow<List<Affirmation>?>(null)
    val uiState: StateFlow<List<Affirmation>?> = _uiSate.asStateFlow()


     fun getCards() {
       _uiSate.value = Datasource().loadAffirmations()
    }
}