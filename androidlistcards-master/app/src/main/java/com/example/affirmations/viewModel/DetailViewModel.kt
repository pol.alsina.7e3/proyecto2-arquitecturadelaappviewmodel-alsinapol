package com.example.affirmations.viewModel

import androidx.lifecycle.ViewModel
import com.example.affirmations.data.Datasource
import com.example.affirmations.model.Affirmation
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow

class DetailViewModel: ViewModel() {
    private val _uiSate = MutableStateFlow<Affirmation?>(null)
    val uiState: StateFlow<Affirmation?> = _uiSate.asStateFlow()
    fun getCard(id:Int){
        val listCard = Datasource().loadAffirmations()
        _uiSate.value = listCard.firstOrNull{ it.Id == id }
    }
}